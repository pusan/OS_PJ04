#include "boundedbuffer.h"

#include "system.h"



BoundedBuffer::BoundedBuffer(int maxsize){

	m_lock = new Lock("buffer is lock");
	m_bufferEmpty = new Condition("buffer is Empty");
	m_bufferFull = new Condition("buffer is Full");
    	m_buffer = (void *)AllocBoundedArray(sizeof(void *) * maxsize);
	m_bufferSize = maxsize;
	m_count = 0;
	m_nextIn = m_nextOut = 0;

}



void BoundedBuffer::Close()

{

      DeallocBoundedArray((char *)m_buffer, m_bufferSize);
      delete m_lock;
      delete m_bufferEmpty;
      delete m_bufferFull;

}



void BoundedBuffer::Read(void* data, int size){

	int i;
	m_lock->Acquire();
	while(m_count == 0)
		{
		m_bufferFull->Wait(m_lock);
		}
	for(i = 0; i < size; i++)
	{
		
		while(m_count == 0)
		{
		m_bufferFull->Wait(m_lock);
		*((char *)data + i) = *((char *)m_buffer + m_nextOut);

			if(i == size - 1)
			{
			printf("%d\n", size);
			m_nextOut = (m_nextOut + 1) % m_bufferSize;
			m_count--;
			m_bufferEmpty->Signal(m_lock);
			}
		}
	}
	
m_lock->Release();

}



void BoundedBuffer::Write(void *data, int size){

	int j;
	m_lock->Acquire();
	while(m_count == m_bufferSize)
	{
		m_bufferEmpty->Wait(m_lock);
	}
	
	for(j = 0; j < size; j++)
	{
	
		while(m_count == m_bufferSize)
		{
		m_bufferEmpty->Wait(m_lock);
		*((char *)m_buffer + m_nextIn) = *((char *)data + j);
		//printf("%c\n",*((char *)data +j));
			if(j == size-1)
			{
			printf("%d\n", size);
			m_nextIn = (m_nextIn+1) % m_bufferSize;
			m_count++;
			m_bufferFull->Signal(m_lock);
			}
		}
	}
	m_lock->Release();
}

